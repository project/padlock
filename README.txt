CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Padlock module provides the ability to lock down form submits in order
 to protect your site from changes durning deployments or testing.


 * For a full description of the module, visit the project page:
   https://drupal.org/project/padlock


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/padlock

REQUIREMENTS
------------
This module does not require any other modules.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - Perform administration tasks for the Padlock project.


     Allows a user to perform administration tasks for the padlock project.


   - Bypass Padlock


     Allows a user to bypass padlock validation.


 * Customize the menu settings in Administration » Configuration and modules »
   Development » Padlock.
